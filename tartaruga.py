import turtle

def triangulo(tartaruga, tamanho):
    for i in range(0,3):
        tartaruga.forward(tamanho)
        tartaruga.left(120)

def triangulo_medio(tartaruga, quant):
    for i in range(0,quant):
        tartaruga.begin_fill()
        triangulo(tartaruga, 50)
        tartaruga.end_fill()
        tartaruga.fd(50)
def triangulo_grande(tartaruga):
    x=4
    while(x>1):
        triangulo_medio(tartaruga, x)
        tartaruga.left(120)
        tartaruga.fd(50)
        x=x-1

def arte():
    window = turtle.Screen()
    window.bgcolor("white")

    tartaruga = turtle.Turtle()
    tartaruga.shape("turtle")
    tartaruga.color("black","red")
    tartaruga.speed(10)

    #primeiro triangulo
    triangulo_grande(tartaruga)

    #segundo triangulo
    tartaruga.fd(-50)
    tartaruga.right(120)
    tartaruga.fd(50)
    triangulo_grande(tartaruga)
    
    #terceiro triangulo

    tartaruga.fd(-50)
    tartaruga.right(120)
    tartaruga.fd(-150)
    tartaruga.right(120)
    triangulo_grande(tartaruga)
    
    window.exitonclick()

arte()
